import { JohPapaPage } from './app.po';

describe('joh-papa App', () => {
  let page: JohPapaPage;

  beforeEach(() => {
    page = new JohPapaPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to papa!');
  });
});
