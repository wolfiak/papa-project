import { browser, by, element } from 'protractor';

export class JohPapaPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('papa-root h1')).getText();
  }
}
