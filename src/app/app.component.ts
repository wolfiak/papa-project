import { Component } from '@angular/core';

@Component({
  selector: 'papa-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'papa';
}
