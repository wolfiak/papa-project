import { Injectable } from '@angular/core';

import { MatSnackBar } from '@angular/material';

@Injectable()
export class ToastService {
  constructor(public sb: MatSnackBar) {}

  onSnackBar(message: string, action: string) {
    this.sb.open(message, action, {
      duration: 2000
    });
  }
}
