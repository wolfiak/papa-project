import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroDataService } from '../hero-data.service';
@Component({
  selector: ' papa-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {
  heroes: Hero[];

  constructor(private heroData: HeroDataService) {}

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes() {
    this.heroData.getHeroes().subscribe(res => {
      return this.heroes = res;
    });
  }
}
