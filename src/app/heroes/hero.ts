export class Hero {
  id: number;
  name: string;
  say: string;
}
